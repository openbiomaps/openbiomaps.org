<?php

/* ***************************************************************************  /
    
GENERAL and GLOBAL FUNCTIONS
Do not change them
Use the local_funcs.php if you need specific functions

****************************************************************************** */
// Postgre SQL functions
function PGconnectSQL($db_user,$db_pass,$db_name,$db_host) {
  #global $db_user, $db_pass, $db_name, $db_host;
  $conn=pg_connect("host=$db_host port=5432 user=$db_user password=$db_pass dbname=$db_name connect_timeout=5");
  pg_set_client_encoding( $conn, 'UTF8' );
  return $conn;
}
function PGquery($ID,$qstr) {
  $res = pg_query($ID,$qstr);
  #print $qstr.'<br>'; 
  return $res;
}
function PGsqlcmd($ID,$qstr) {
   $res = pg_query($ID,$qstr);
   $nur = pg_affected_rows($res);
   return $nur;
}
function PGnext_arow($res) {
  $row = pg_fetch_assoc($res);
  return $row;
}
// split ::: separated string
// the second one the default language
// improvment required!!!
// 1: more language support
// 2: generalized language set
function sepLangText($text) {
    global $LANG;
    if (preg_match('/:::/',$text)) {
       list($text_1,$text_2) = preg_split('/:::/',$text);
       if ($LANG == 'hu') $text = $text_1;
       else $text = $text_2;
    }
    return $text;
}
/* utf8 ucfirst
 * glue arbitray arguments into a string
 * */
function t() {
    $encoding='UTF-8';
    mb_internal_encoding($encoding);
    $s = func_get_args();
    $string = implode(' ',$s);
    if ($string=='') {
        return str_undefined;
    }
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    if ($firstChar!='')
        return mb_strtoupper($firstChar, $encoding) . $then;
    else {
        if (is_array($s)) return "";
        else return $s;
    }
}
function quote($text) {
    if ($text=='NULL' or trim($text)=='') return "NULL";

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $random_delimeter = substr( str_shuffle( $chars ), 0, 16 );
    $text = '$'.$random_delimeter.'$'.$text.'$'.$random_delimeter.'$';

    return $text;
}
// random color
// Creates a random color string
// return a 6byte long hex string like AABBCC
function rand_color() {
    $rc = '';
    for ($i=0;$i<3;$i++) {
        $rnd = dechex(rand(0,255));
        if (strlen($rnd) == 1 ) $rnd = "0$rnd";
        $rc .= $rnd;
    }
    return $rc;
}
/* A simple web-get function */
function wget($url,$header) {
    $curl = curl_init();
    if ($curl) {
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $result = curl_exec($curl);
        curl_close($curl);
    } else {
        $result = common_message('error','Curl error');
    }
    return $result;
}
/* Validate JSON
 * used in geomtest.php
 * */
function is_json($str){
    return json_decode($str) != null;
}
?>
