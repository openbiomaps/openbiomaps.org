<?php

$dir = "files";

// Run the recursive function 

$response = scan($dir);

// This function scans the files folder recursively, and builds a large array

function scan($dir){

	$files = array();

	// Is there actually such a folder/file?

	if(file_exists($dir)){
	
            $u = scandir($dir); 
            if (isset($_GET['sort']) and $_GET['sort']=='time')
                usort($u, cmp_sort($dir));
            foreach ($u as $f) {
		//foreach(scandir($dir) as $f) {
		
			if(!$f || $f[0] == '.') {
				continue; // Ignore hidden files
			}

			if(is_dir($dir . '/' . $f)) {

				// The path is a folder

				$files[] = array(
					"name" => $f,
					"type" => "folder",
					"path" => $dir . '/' . $f,
					"items" => scan($dir . '/' . $f) // Recursively get the contents of the folder
				);
			}
			
			else {

				// It is a file

				$files[] = array(
					"name" => $f,
					"type" => "file",
					"path" => $dir . '/' . $f,
					"size" => filesize($dir . '/' . $f), // Gets the size of this file
                                        "time" => date ("Y-m-d H:i",filemtime($dir . '/' . $f))
				);
			}
		}
	
	}
        
	return $files;
}

// reverse sort by time
function cmp_sort ($dir) {
    return function ($a, $b) use ($dir) {
        //return strnatcmp($a[$key], $b[$key]);
        return filemtime("$dir/$b") - filemtime("$dir/$a");
    };
}


// Output the directory listing as JSON

header('Content-type: application/json');

echo json_encode(array(
	"name" => "files",
	"type" => "folder",
	"path" => $dir,
	"items" => $response
));
