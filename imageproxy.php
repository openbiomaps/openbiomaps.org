<?php

if ( $_SERVER['SERVER_NAME'] != 'openbiomaps.org' ) exit;

$PROXY = 'https://openbiomaps.org/imageproxy.php?url=';

# This script forwards the call

# Check for url parameter, and prevent file transfer
if (isset($_GET['url']) and preg_match('#^https?://#', $_GET['url']) === 1) {
    $url = $_GET['url'];
} else {
    header('HTTP/1.1 404 Not Found');
    exit;
}

# Check if the client already has the requested item
if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) or 
    isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
    header('HTTP/1.1 304 Not Modified');
    exit;
}



$WIDTH = isset($_GET['w']) ? $_GET['w'] : 600;

//$tmp_file = tempnam(sys_get_temp_dir(), md5($url) . $WIDTH . '.jpg');
$tmp_file = '/var/www/html/biomaps/img/dynamic/' . md5($url) . $WIDTH . '.jpg';

if (file_exists($tmp_file)) {
    $image = imagecreatefromstring(file_get_contents($tmp_file));

    $imgResized = $image;
} else {
    $image = imagecreatefromstring(file_get_contents($url));
    $width = imagesx($image);
    $height = imagesy($image);
    if ($width > $WIDTH)
        $imgResized = imagescale($image , $WIDTH, -1);
    else
        $imgResized = $image;
    // Output cache/tmp file
    imagejpeg($imgResized, $tmp_file);
}

if (is_resource($image) === true) {

    header('Content-Type: image/jpeg');

    // Output the image
    imagejpeg($imgResized);

    imagedestroy($imgResized);
}
exit;


/* Not works....
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_USERAGENT, 'Searsia/1.0');
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
curl_setopt($ch, CURLOPT_BUFFERSIZE, 12800);
curl_setopt($ch, CURLOPT_NOPROGRESS, false);
curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function($DownloadSize, $Downloaded, $UploadSize, $Uploaded) { return ($Downloaded > 1024 * 512) ? 1 : 0; } ); # max 500kb
$out = curl_exec ($ch);
curl_close ($ch);


$file_array = explode("\r\n\r\n", $out, 2);
$header_array = explode("\r\n", $file_array[0]);
foreach($header_array as $header_value) {
    $header_pieces = explode(': ', $header_value);
    if(count($header_pieces) == 2) {
        $headers[$header_pieces[0]] = trim($header_pieces[1]);
    }
}
if (array_key_exists('Location', $headers)) {
    $newurl = urlencode($headers['Location']);
    header("HTTP/1.1 301 Moved Permanently"); 
    header('Location: ' . $PROXY . $newurl);
} else {
    if (array_key_exists('Content-Type', $headers)) {
        $ct = $headers['Content-Type'];
        if (preg_match('#image/png|image/.*icon|image/jpe?g|image/gif#', $ct) !== 1) {
            header('HTTP/1.1 404 Not Found');
            exit;
        }
        header('Content-Type: ' . $ct);
    }
    if (array_key_exists('Content-Length', $headers))
        header('Content-Length: ' . $headers['Content-Length']);
    if (array_key_exists('Expires', $headers))
        header('Expires: ' . $headers['Expires']);
    if (array_key_exists('Cache-Control', $headers))
        header('Cache-Control: ' . $headers['Cache-Control']);
    if (array_key_exists('Last-Modified', $headers))
        header('Last-Modified: ' . $headers['Last-Modified']);
    echo $file_array[1];
}
 */

?>
