<?php

// This is the composer autoloader. Used by
// the markdown parser and RSS feed builder.
require 'vendor/autoload.php';

// Explicitly including the dispatch framework,
// and our functions.php file
require 'bblog/app/includes/functions.php';
define('site_url','http://openbiomaps.org/blog/');
define('blog_description',"This is an OpenBioMaps blog</a>.");
define('blog_authorbio',"");
define('posts_perpage',5);

if(!isset($_GET['page'])) $page = 1;
else $page = $_GET['page'];

$page = $page ? (int)$page : 1;
	
$posts = get_posts($page);
	
if(empty($posts) || $page < 1){
    // a non-existing page
    not_found();
}
  $has_pagination = has_pagination($page);

  ob_start();
  include "app/views/main.html.php";
  $content = trim(ob_get_clean());
  
  $layout = "app/views/layout.html.php";

  //header('Content-type: text/html; charset=utf-8');

  ob_start();
  require $layout;
  echo trim(ob_get_clean());


// The post page
/*get('/:year/:month/:name',function($year, $month, $name){

	$post = find_post($year, $month, $name);

	if(!$post){
		not_found();
	}

	render('post',array(
		'title' => $post->title .' ⋅ ' . config('blog.title'),
		'p' => $post
	));
});*/

// The JSON API
/*get('/api/json',function(){

	header('Content-type: application/json');

	// Print the 10 latest posts as JSON
	echo generate_json(get_posts(1, 10));
});*/

// Show the RSS feed
/*get('/rss',function(){

	header('Content-Type: application/rss+xml');

	// Show an RSS feed with the 30 latest posts
	echo generate_rss(get_posts(1, 30));
});*/


// If we get here, it means that
// nothing has been matched above

/*get('.*',function(){
	not_found();
});*/

